import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import {AuthService} from '../../service/auth.service'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})



export class LoginComponent implements OnInit {


  login = new FormGroup({
    user: new FormControl(''),
    pass: new FormControl(''),
  });

  

  constructor(private formBuilder: FormBuilder, private auth:AuthService) {}


  ngOnInit() {
    this.login = this.formBuilder.group({
      user: ['',Validators.required],
      pass: ['', Validators.required],
    });

  }

  public onloginUser(value){
    console.log(value)
    this.auth.logIn( value.user, value.pass)
  }
  
}
