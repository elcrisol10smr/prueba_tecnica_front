import { Component, OnInit } from '@angular/core';
import {ProdService} from '../../service/prod.service'
@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.component.html',
  styleUrls: ['./carrito.component.css']
})
export class CarritoComponent implements OnInit {

  public items;
  public total=0;
  constructor(private service:ProdService) { }

  ngOnInit() {
    this.service.getCarrito().subscribe(data => {
      this.items=data[0].order 
      console.log(this.items)
      for (let i = 0; i < Object.keys(this.items).length; i++){
        console.log(i)
        this.total = this.total + this.items[i].product.price
      }
     
      console.log('precio',this.total)
      console.log(this.items)
    })
  }

}
