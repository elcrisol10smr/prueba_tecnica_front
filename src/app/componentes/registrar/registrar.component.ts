import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import {AuthService} from '../../service/auth.service'

@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.component.html',
  styleUrls: ['./registrar.component.css']
})
export class RegistrarComponent implements OnInit {

  registro = new FormGroup({
    email: new FormControl(''),
    user: new FormControl(''),
    pass: new FormControl(''),
  });

  

  constructor(private formBuilder: FormBuilder, private auth:AuthService) {}


  ngOnInit() {
    this.registro = this.formBuilder.group({
      email: ['',Validators.required],
      user: ['',Validators.required],
      pass: ['', Validators.required],
    });

  }

  public registroUser(value){
    console.log(value)
    this.auth.RegisterIn(value.user, value.email, value.pass)
  }

}
