import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubirproductosComponent } from './subirproductos.component';

describe('SubirproductosComponent', () => {
  let component: SubirproductosComponent;
  let fixture: ComponentFixture<SubirproductosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubirproductosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubirproductosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
