import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import {ProdService} from '../../service/prod.service'

@Component({
  selector: 'app-subirproductos',
  templateUrl: './subirproductos.component.html',
  styleUrls: ['./subirproductos.component.css']
})
export class SubirproductosComponent implements OnInit {
  public categorias;

  subir = new FormGroup({
    nombre:new FormControl(''),
    descripcion:new FormControl(''),
    activo:new FormControl(''),
    precio:new FormControl(''),
    categoria:new FormControl(''),
  });

  

  constructor(private formBuilder: FormBuilder, private prodservice:ProdService) {}


  ngOnInit() {
    this.categorias_lista()
    this.subir = this.formBuilder.group({
      nombre: ['',Validators.required],
      descripcion: ['', Validators.required],
      activo:['', Validators.required],
      precio:['', Validators.required],
      categoria:['', Validators.required],
    });

  }

  categorias_lista(){
    this.prodservice.getCategoria().subscribe(data=>{
      this.categorias=data
    })
  }
  public agregar(value){
    console.log('agregar',value.categoria[0])
    this.prodservice.postProducto( value.nombre, value.descripcion, value.precio, value.activo, value.categoria[0])
  }
  
}

