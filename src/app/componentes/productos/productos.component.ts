import { Component, OnInit } from '@angular/core';
import {ProdService} from '../../service/prod.service'

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent implements OnInit {
  public items;
  public datos;
  public categorias;
  constructor(private service:ProdService) { }

  ngOnInit() {
    this.listarproductos()
    this.categorias_lista()
  }

  listarproductos(){
    this.service.getLists().subscribe(data => {
      this.items=data
      this.datos=data
      console.log(data)
    })
  }
  productos(products){
    this.service.postCarrito(products)
  }

  categorias_lista(){
    this.service.getCategoria().subscribe(data=>{
      this.categorias=data
    })
  }

  categoria_filter(category: string){
    this.items=this.datos;
    this.items = this.items.filter(item => item.categories.name == category);
    console.log(this.items)
  }
}
