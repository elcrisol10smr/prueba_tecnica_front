import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  url_api: string = "http://localhost:8000/"
  headers: {};
  onErrorChanged: Subject<any>;


  constructor(private http: HttpClient, private router: Router) { 
    this.headers = {
      'Content-Type': 'application/json',
    }

  }
  
  logIn(username: string, password: string) {
    this.http.post(this.url_api + "api/users/token/", { username: username, password: password }, { headers: this.headers })
      .subscribe(
        response => {
          localStorage.setItem('accessToken', response['access']);
          this.headers['Authorization'] = 'Bearer ' + response['access'];
          console.log('auntentifico')
          this.router.navigate(["/productos"]);
        },
        error => {
          console.log(error);
          this.onErrorChanged.next(error.error.non_field_errors[0]);
        }
      )
  }

  RegisterIn(username: string,email:string, password: string)
  {
    this.http.post(this.url_api + "api/users/sign-up", { username: username,email:email, password: password }, { headers: this.headers }).subscribe(
      response => {
        
        this.router.navigate(["/login"]);
      })
  }

  logoutUser() {
    localStorage.clear();
    this.http.post(this.url_api + `api/users/token/logout?access_token=${localStorage.getItem("accessToken")}`, { headers: this.headers })
      .subscribe(response => {
        console.log(response)
      })
  }


}
