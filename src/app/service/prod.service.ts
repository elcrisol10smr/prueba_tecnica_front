import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ProdService {
  url_api: string = "http://localhost:8000/"
  headers: {};
  headers1: {};
  onErrorChanged: Subject<any>;

  constructor( private http: HttpClient) {
    this.headers = {
      'Content-Type': 'application/json',
    }
    this.headers1 = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
    }

  }

  getLists(): any {
    return this.http.get(this.url_api + "api/ecommerce/products-list", { headers: this.headers })
  }

  postCarrito(product:number){
    this.http.post(this.url_api + "api/ecommerce/create-order",{ pk:product, quantity:1 }, { headers: this.headers1 }).subscribe(e => console.log(e))
  }

  getCarrito(): any {
    return this.http.get(this.url_api + "api/ecommerce/shoping-cart-items", { headers: this.headers1 })
  }

  getCategoria(): any {
    return this.http.get(this.url_api + "api/ecommerce/category-list",{ headers: this.headers1 })
  }

  postProducto(title,description,price,avaiable, categories){
    this.http.post(this.url_api + "api/ecommerce/products-create",{ title:title,description:description,price:price,avaiable:avaiable, category_pk:categories}, { headers: this.headers1 }).subscribe(e => console.log(e))
  }

}
